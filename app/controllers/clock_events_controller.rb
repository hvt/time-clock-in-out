class ClockEventsController < ApplicationController
  def index
    render :index
  end

  def create
    user = User.find_by(email: params[:email])
    if user.present? && user.try(:authenticate, params[:password])
      clock_event = ClockEvent.where(clock_out: [nil], user: user).last
      if clock_event.present?
        clock_event.update(clock_out: Time.now)
        flash[:notice] = "You have clocked-out!"
      else
        user.clock_events.create(clock_in: Time.current)
        flash[:notice] = "You have clocked-in!"
      end
    else
      flash[:notice] = "You were not authenticated"
    end
    redirect_to time_index_path
  end

end
