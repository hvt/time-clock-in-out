class TimeController < ApplicationController
  def index
    @time = Time.now.strftime("%H:%M:%S %P").in_time_zone('Eastern Time (US & Canada)').strftime("%H:%M:%S %P")
    @clock_events = ClockEvent.all
  end

  def get_time
    @time = Time.now.getlocal('-05:00').strftime("%I:%M:%S").in_time_zone('Eastern Time (US & Canada)').strftime("%H:%M:%S %p")
    render json: @time
  end
end
