class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  has_secure_password
  validates :email, presence: true, uniqueness: true

  has_many :clock_events

  def clock_event
    @clock_event ||= clock_events.incomplete.last || clock_events.new
  end

end
