class ClockEvent < ApplicationRecord
  belongs_to :user
  scope :last_open_clock_event, -> { where(clock_out: [nil]).last }
  scope :complete, -> { where.not(clock_out: nil) }

  before_save :hours_worked

  def hours_worked
    if clock_in.present? && clock_out.present?
      self.hours_worked = (clock_out-clock_in)/60/60
    end
  end
end
