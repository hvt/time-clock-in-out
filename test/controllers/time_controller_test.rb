require 'test_helper'

class TimeControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get time_index_url
    assert_response :success
  end

  test "should get get_time" do
    get time_get_time_url
    assert_response :success
  end

end
