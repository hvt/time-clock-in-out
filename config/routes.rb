Rails.application.routes.draw do
  resource :clock_events

  get 'clock_events/index' => 'clock_events#index'
  get 'time/index' => 'time#index'
  get 'time/get_time' => 'time#get_time'
  root to: 'users#new'
  # sign up page with form:
	get 'users/new' => 'users#new', as: :new_user

	# create (post) action for when sign up form is submitted:
	post 'users' => 'users#create'
end
