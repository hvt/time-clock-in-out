# README

* Please begin by creating an account with email and password.

  `Go to the home route : https://fast-basin-21096.herokuapp.com`

* Go to clock-in route, `https://fast-basin-21096.herokuapp/time/index`

* Once user is created, add email and password, and click Clock In/Clock Out button.

* After some time, re-enter email and password to clock-out on the time clock.

* Add as many users from the home route to keep record of users clock-in and clock-out times.
