class AddHoursWorkedToClockEvent < ActiveRecord::Migration[5.2]
  def change
    add_column :clock_events, :hours_worked, :integer
  end
end
