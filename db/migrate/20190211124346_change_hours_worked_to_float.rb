class ChangeHoursWorkedToFloat < ActiveRecord::Migration[5.2]
  def change
    change_column :clock_events, :hours_worked, :float
  end
end
