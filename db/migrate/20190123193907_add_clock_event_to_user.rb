class AddClockEventToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :clock_event_id, :integer
  end
end
