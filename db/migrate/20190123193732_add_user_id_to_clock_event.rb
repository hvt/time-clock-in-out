class AddUserIdToClockEvent < ActiveRecord::Migration[5.2]
  def change
    add_column :clock_events, :user_id, :integer
  end
end
